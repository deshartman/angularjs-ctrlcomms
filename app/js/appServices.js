angular.module('AppServices', [])

    .factory('appServices', function ($rootScope) {   // $rootScope for the entire Window.
        // public
        this.message = '';

        // public
        this.messageEvent = function (msg) {

            this.message = msg; // Set the value to what was passed in
            console.log(this.message);
            $rootScope.$broadcast('messageChangeEvent');   // Send out an Event in the rootScope
        };

        return this;
    });
