var ctrlCommModule = angular.module('CtrlCommModule',['AppServices','Controller1','Controller2'])

        .config(function ($routeProvider) {

            // Configure the routes
            $routeProvider
                .when('/view1', {
                    templateUrl:'partials/view1.html',
                    controller:'controller1'
                })
                .when('/view2', {
                    templateUrl:'partials/view2.html',
                    controller:'controller2'
                })
                .otherwise({
                    redirectTo:'/view1'
                });
        });
