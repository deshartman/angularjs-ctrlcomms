angular.module('Controller2', [])

    .controller('controller2', function ($scope, appServices) {

        console.log('check messageChangeEvent with message: ' + appServices.message);

        $scope.$on('messageChangeEvent', function () {  // Listen for the messageChangeEvent Event
            console.log('Got the messageChangeEvent with message: ' + appServices.message);
            $scope.message = "2: " + appServices.message;
        });

        $scope.checkMessage = function() {
            $scope.message = "2: " + appServices.message;
        }
    });

