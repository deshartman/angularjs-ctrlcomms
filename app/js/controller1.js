angular.module('Controller1', [])

    .controller('controller1', function ($scope, $location, appServices) {
        $scope.handleClick = function (msg) {

            console.log('Sending out message: ' + msg);
            appServices.messageEvent(msg);
            // Change the screen
            $location.path("/view2");
        }
    });

